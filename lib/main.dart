import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const Idtest());
}

class Idtest extends StatefulWidget {
  const Idtest({ Key? key }) : super(key: key);

  @override
  _IdtestState createState() => _IdtestState();
}

class _IdtestState extends State<Idtest> {

String deviceName = '', deviceVersion = '', deviceIdentifier = '';

 Future<void> _getDeviceDetails() async {
   final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

   try {
     if(Platform.isAndroid){
       var build = await deviceInfoPlugin.androidInfo;
       setState(() {
         deviceName = build.model;
         deviceVersion = build.version.toString();
         deviceIdentifier = build.androidId;
       });
     }
     else if(Platform.isIOS){
       var data = await deviceInfoPlugin.iosInfo;
       setState(() {
         deviceName = data.name;
         deviceVersion = data.systemVersion;
         deviceIdentifier = data.identifierForVendor;
       });
     }
   }on PlatformException{
     print('Failed to get platform version');
   }


 }


  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white, 
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: const Text(
            "Teste Unique Device ID Flutter",
            style: TextStyle(
              color: Colors.black,
            ),
            ),
        ),
        body:  Center(
          child: Column(
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const SizedBox(
                height: 150,
              ),
                Text('Device Name: -'+ deviceName),
               const SizedBox(
                 height: 30,
               ),
                Text('Device Version: - '+ deviceVersion),
               const SizedBox(
                 height: 30,
               ),
                Text('Device Identifier: - '+ deviceIdentifier),
               const SizedBox(
                 height: 30,
               ),
              TextButton(
                onPressed: (){
                  _getDeviceDetails();
                }, 
                child: const Text("Get Device Details"),
                ),
            ],
          ),
        ),
        ),
     
        
   
    );
  }
}